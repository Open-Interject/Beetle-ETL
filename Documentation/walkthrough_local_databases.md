# Beetle and Local Databases

Beetle can be used to transfer data from local database in addition to remote ones. Here we will walk through how to setup beetle to move data from a locally hosted Mongo database to a locally hosted SQLite database.

## Step 1: install mongodb

In order to run Beetle with a local database, you will need to install mongodb so you have a database to connect to. Information about install mongo can be found here [Install MongoDB](https://docs.mongodb.com/manual/installation/)

## Step 2: install SQL (optional)

If you wish to use Beetle to pull from mongo and push to SQL then you will also need to install SQL. 

> NOTE: we have tested our program with SQL Server but not with other versions of SQL such as Postgres or Oracle

A free version of SQL server can be found [here](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express)

If you are trying to setup a local data SQL server database it is important to ensure during installation to specify the install as a "default instance". This is because connecting to a named instance in SQL Server requires using the format, `host\instanceName`. This will cause a serious issue with pyodbc resulting in the inability to connect. As far as we know this is an issue caused by drivers not python or SQL Server.


## Step 3: setup MongoDB and SQL databases

Before you can connect to a database you need to setup a database in mongodb and SQL Server if you are using it. This is easily accomplished using the [MongoDB Database Viewer, Compass](https://www.mongodb.com/products/compass). Respectively, [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017) can be used to navigate amen manage SQL databases.

## Step 4: setup Beetle Config

Assuming you have Beetle installed you are ready to connect to a local database. The only section that needs to change in a config file is the connection info section. Below is an example of a connectionInfo section adjusted for local databases

```json
"connectionInfo": {
    "useWindowsAuth": true,
    "sqlServerDriver": "ODBC Driver 13 for SQL Server",
    "sqlServerUser": "testuser",
    "sqlServerPass": "testpass",
    "sqlServerDatabase": "Database1",
    "sqlServerHost": "localhost",
    "sqlServerPort": "",
    "customMongoURI": "",
    "mongoServerUser": "",
    "mongoServerHost": "localhost",
    "mongoServerPass": "",
    "mongoServerPort": 27017,
    "mongoDatabase": "Database1",
    "useMongoURISRV": false,
    "mongoCollection": "Collection1"
}
``` 

> NOTE: it is important to set, *useMongoURISRV* to false, because that indicated a database hosted by MongoDB

## Step 5: Finish mapping

After the previous steps are complete the rest of the beetle config can be setup according to the user's desired transformation process and then run.